import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './user/login/login.component';
import { BrowseComponent } from './game/browse/browse.component';
import { HomeComponent } from './home/home/home.component';
import { GameComponent } from './game/game/game.component';
import { AddComponent } from './game/add/add.component';
import { AuthGuard } from './game/helpers/auth-guard';
import { RegisterComponent } from './user/register/register.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'browse', component: BrowseComponent, canActivate: [AuthGuard] },
  { path: 'add', component: AddComponent },
  { path: 'home', component: HomeComponent },
  { path: 'browse/:id', component: GameComponent },
  { path: 'register', component: RegisterComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }