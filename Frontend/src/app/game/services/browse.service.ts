import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Game } from '../models/Game';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Filter } from '../helpers/filter';
import { Message } from '../helpers/message';
import { Company } from '../models/Company';
import { ReviewDto } from '../models/dtos/ReviewDto';

@Injectable({
  providedIn: 'root'
})
export class BrowseService {
  private browseUrl = 'https://localhost:44364/';

  constructor(private http: HttpClient) { }

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>('/api/game/all');
  }
  getGame(id: number): Observable<Game> {
    return this.http.get<Game>('/api/game/' + id.toString());
  }
  getFilteredGames(filter: Filter): Observable<Game[]> {
    var url = this.composeQueryString(filter);
    return this.http.get<Game[]>('/api/game/filtered' + url);
  }
  addGame(game: Game): Observable<Message> {
    return this.http.post<Message>('/api/game/add', game);
  }
  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>('/api/company/all')
  }
  getReviews(id: number): Observable<ReviewDto[]> {
    return this.http.get<ReviewDto[]>('api/game/reviews/' + id.toString())
  }


  composeQueryString(object: any): string {
    let result = '';
    let isFirst = true;
    if (object) {
      Object.keys(object)
        .filter(key => object[key] !== null && object[key] !== undefined)
        .forEach(key => {
          let value = object[key];
          if (value instanceof Date) {
            value = value.toISOString();
          }
          if (isFirst) {
            result = '?' + key + '=' + value;
            isFirst = false;
          } else {
            result += '&' + key + '=' + value;
          }
        });
    }
    return result;
  }
}