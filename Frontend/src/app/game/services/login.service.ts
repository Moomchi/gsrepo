import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from "rxjs";
import { LoginResponse } from "../helpers/LoginResponse";
import { Message } from "../helpers/message";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private jwtHelper: JwtHelperService, private http: HttpClient) { }

  isUserAuthenticated(): boolean {
    const token: string | null = localStorage.getItem("jwt");
    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    }
    else {
      localStorage.removeItem("jwt");
      return false;
    }
  }
  register(credentials: any): Observable<Message> {
    return this.http.post<Message>('api/registration/create', credentials);
  }
  login(credentials: any): Observable<LoginResponse> {
    return this.http.post<LoginResponse>("api/login", credentials);
  }
}