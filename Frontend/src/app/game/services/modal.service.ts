import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Game } from '../models/Game';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Message } from '../helpers/message';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private browseUrl = 'https://localhost:44364/';

  constructor(private http: HttpClient) { }

  deleteGame(id: number): Observable<Message> {
    return this.http.delete<Message>('api/game/delete/' + id.toString());
  }
  editGame(game: Game): Observable<Message> {
    return this.http.put<Message>('api/game/edit', game);
  }


}