import { UserWithGamesReviews } from "../models/dtos/UserWithGamesReviews";
import { Message } from "./message";

export class LoginResponse extends Message {
  token: string;
  userDto: UserWithGamesReviews;
}