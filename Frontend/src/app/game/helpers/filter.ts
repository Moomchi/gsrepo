export interface Filter {
  name?: string;
  companyName?: string;
  releaseYear?: string;
  price?: number;
}
