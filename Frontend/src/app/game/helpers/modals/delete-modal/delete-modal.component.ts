import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from 'src/app/game/services/modal.service';
import { Message } from '../../message';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {
  @Input() element: any;
  message: Message = new Message();

  constructor(private modalService: NgbModal,
    private customModalService: ModalService,
    private router: Router) { }

  ngOnInit(): void {
  }

  open(content: any) {
    this.modalService.open(content);
  }

  delete(): void {
    this.customModalService.deleteGame(this.element.id).subscribe(message => {
      this.message = message;
      if (this.message.code === 0) {
        this.redirect();
      }
    });
  }

  redirect(): void {
    this.router.navigate(['/browse']);
  }

}
