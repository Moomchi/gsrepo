import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { EditModalComponent } from './edit-modal/edit-modal.component';

@NgModule({
  imports: [BrowserModule, NgbModule],
  declarations: [DeleteModalComponent, EditModalComponent],
  exports: [DeleteModalComponent, EditModalComponent],
  bootstrap: [DeleteModalComponent, EditModalComponent]
})
export class NgbdModalBasicModule { }
