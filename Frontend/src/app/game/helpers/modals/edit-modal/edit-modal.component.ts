import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from 'src/app/game/services/modal.service';
import { Message } from '../../message';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css']
})
export class EditModalComponent implements OnInit {
  @Input() element: any;

  exists: boolean = false;
  message: Message = new Message();

  constructor(private modalService: NgbModal,
    private customModalService: ModalService,
    private router: Router) { }

  ngOnInit(): void {
  }

  open(content: any) {
    this.modalService.open(content);
  }

  edit(): void {
    this.customModalService.editGame(this.element).subscribe(message => {
      this.message = message;
      this.exists = false;
      if (this.message.code === 5) {
        this.redirect();
        this.modalService.dismissAll();
      }
      if (this.message.code === 4) {
        this.exists = true;
      }
    });
  }
  redirect(): void {
    this.router.navigate(['/browse']);
  }
}
