export enum Role {
  client,
  publisher,
  admin
}