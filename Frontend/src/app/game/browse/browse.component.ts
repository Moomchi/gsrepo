import { Component, OnInit } from '@angular/core';

import { Game } from '../models/Game';

import { BrowseService } from '../services/browse.service';
import { Filter } from '../helpers/filter'
import { Company } from '../models/Company';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.css']
})
export class BrowseComponent implements OnInit {
  sortedAsc = true;
  currentSort = "";
  games: Game[] = [];
  filter: Filter = { name: '', companyName: '', releaseYear: '' };
  years: string[] = [];
  companies: Company[] = [];

  constructor(private browseService: BrowseService) { }

  ngOnInit(): void {
    this.getAll();


  }

  getAll(): void {
    this.browseService.getGames()
      .subscribe(games => {
        this.games = games;
        games.forEach(element => {
          this.years.push(element.releaseYear.toString());
        });
        this.currentSort = "#";
        this.sort();
      });
    this.browseService.getCompanies()
      .subscribe(companies => {
        this.companies = companies;
      })
  }

  sort(): void {
    switch (this.currentSort) {
      case "#": {
        if (!this.sortedAsc) {
          this.games.sort((a, b) => a.id - b.id);
          this.sortedAsc = !this.sortedAsc;
        } else {
          this.games.sort((a, b) => b.id - a.id);
          this.sortedAsc = !this.sortedAsc;
        }
        break;
      }
      case "Name": {
        if (!this.sortedAsc) {
          this.games.sort((a, b) => a.name.localeCompare(b.name));
          this.sortedAsc = !this.sortedAsc;
        } else {
          this.games.sort((a, b) => b.name.localeCompare(a.name));
          this.sortedAsc = !this.sortedAsc;
        }
        break;
      }
      case "Company": {
        if (!this.sortedAsc) {
          this.games.sort((a, b) => a.company.name.localeCompare(b.company.name));
          this.sortedAsc = !this.sortedAsc;
        } else {
          this.games.sort((a, b) => b.company.name.localeCompare(a.company.name));
          this.sortedAsc = !this.sortedAsc;
        }
        break;
      }
      case "Price": {
        if (!this.sortedAsc) {
          this.games.sort((a, b) => a.price - b.price);
          this.sortedAsc = !this.sortedAsc;
        } else {
          this.games.sort((a, b) => b.price - a.price);
          this.sortedAsc = !this.sortedAsc;
        }
        break;
      }
      case "Release Year": {
        if (!this.sortedAsc) {
          this.games.sort((a, b) => a.releaseYear - b.releaseYear);
          this.sortedAsc = !this.sortedAsc;
        } else {
          this.games.sort((a, b) => b.releaseYear - a.releaseYear);
          this.sortedAsc = !this.sortedAsc;
        }
        break;
      }
    }
  }

  search(): void {
    var newFilter = this.filter;
    if (this.filter.companyName === 'All Companies') {
      newFilter.companyName = '';
    }
    if (this.filter.releaseYear === 'All Years') {
      newFilter.releaseYear = '';
    }

    this.browseService.getFilteredGames(newFilter)
      .subscribe(games => {
        this.games = games;
        this.currentSort = "#";
        this.sortedAsc = true;
        this.sort();
      });
  }
}
