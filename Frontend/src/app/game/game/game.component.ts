import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReviewDto } from '../models/dtos/ReviewDto';
import { Game } from '../models/Game';
import { BrowseService } from '../services/browse.service';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit, OnDestroy {
  private sub: any;

  id: number = 0;
  game: Game = new Game();
  reviews: ReviewDto[] = [];
  reviewsEmpty = false;


  constructor(private route: ActivatedRoute,
    private browseService: BrowseService) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.getGame();

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getGame(): void {
    this.browseService.getGame(this.id)
      .subscribe(game => {
        this.game = game;
      });
    this.browseService.getReviews(this.id)
      .subscribe(reviews => {
        this.reviews = reviews;
        if (reviews === null) {
          this.reviewsEmpty = true;
        }
      });
  }
}
