import { Nation } from './Nation';

export interface Company {
  id: number;
  name: string;
  nation: Nation;
}