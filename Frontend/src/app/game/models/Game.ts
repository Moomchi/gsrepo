import { Company } from './Company';
import { ReviewDto } from './dtos/ReviewDto';

export class Game {
  id: number;
  name: string;
  description: string;
  releaseYear: number;
  price: number;
  company: Company;
  companyId: number;
  logoUrl: string;
  reviews: ReviewDto[];
}