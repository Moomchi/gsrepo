import { Role } from "../../helpers/role";

export class UserDtoSmall {
  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  age: number;
  email: string;
  role: Role;
}