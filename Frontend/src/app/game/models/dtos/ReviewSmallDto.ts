export class ReviewSmallDto {
  id: number;
  text: string;
  gameId: number;
}