import { UserDtoSmall } from "./UserDtoSmall";

export class ReviewDto {
  id: number;
  text: string;
  userDtoSmall: UserDtoSmall;
}