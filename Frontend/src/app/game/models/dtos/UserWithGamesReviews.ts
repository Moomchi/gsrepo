import { ReviewDto } from "./ReviewDto";
import { UserDtoSmall } from "./UserDtoSmall";

export class UserWithGamesReviews extends UserDtoSmall {
  reviewSmallDto: ReviewDto[];
  gameIds: number[];
}