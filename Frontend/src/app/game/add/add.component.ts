import { getSupportedInputTypes } from '@angular/cdk/platform';
import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from '../helpers/message';
import { Game } from '../models/Game';
import { BrowseService } from '../services/browse.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  @Input() element: any;
  @Input() inModal: boolean;
  @Input() nameExists: boolean;
  @Input() message: Message;

  game: Game = new Game();
  publisher = "Some publisher";
  maxYear: number = new Date().getFullYear() + 10;
  maxPrice: number = 200;
  validate: boolean = false;
  toggle: boolean = false;

  constructor(private browseService: BrowseService,
    private router: Router) { }

  ngOnInit(): void {
    if (this.element) {
      this.game = this.element
    }
  }

  submit(): void {
    if (this.toggle) {
      this.toggle = !this.toggle;
    }

    this.game.companyId = 4;
    this.browseService
      .addGame(this.game)
      .subscribe(message => {
        this.message = message;
        if (this.message.code === 1) {
          this.redirect();
        }
        this.toggle = !this.toggle;
      });
  }
  redirect(): void {
    this.router.navigate(['/browse']);
  }
}
