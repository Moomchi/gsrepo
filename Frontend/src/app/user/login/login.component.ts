import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router'
import { LoginResponse } from 'src/app/game/helpers/LoginResponse';
import { LoginService } from 'src/app/game/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  response = new LoginResponse();

  constructor(private router: Router,
    private loginService: LoginService) { }

  ngOnInit(): void {
  }

  login(form: NgForm) {
    const credentials = {
      'Username': form.value.username,
      'Password': form.value.password

    }
    this.loginService.login(credentials)
      .subscribe(response => {
        this.response = response;
        if (response.code === 0) {
          localStorage.setItem("jwt", response.token);
          localStorage.setItem("first_name", response.userDto.firstName)
          this.router.navigate(["/"]);
        }
      })
  }
}
