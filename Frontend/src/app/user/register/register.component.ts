import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Message } from 'src/app/game/helpers/message';
import { LoginService } from 'src/app/game/services/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  response = new Message();
  constructor(private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
  }

  register(form: NgForm) {
    const credentials = {
      'FirstName': form.value.firstname,
      'MiddleName': form.value.middlename,
      'LastName': form.value.lastname,
      'Username': form.value.username,
      'Age': form.value.age,
      'Email': form.value.email,
      'Password': form.value.password,
      'Password2': form.value.password2
    }
    if (form.invalid) {
      this.response.text = "Invalid input!";
    } else {
      this.loginService.register(credentials)
        .subscribe((message: any) => {
          this.response = message;
          console.log(message.text);
          if (message.code === 0) {
            this.router.navigate(["login"]);
          }
        });
    }
  }

}
