import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginResponse } from 'src/app/game/helpers/LoginResponse';
import { UserWithGamesReviews } from 'src/app/game/models/dtos/UserWithGamesReviews';
import { LoginService } from 'src/app/game/services/login.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  signed: boolean;
  firstname: string | null;
  constructor(private router: Router, private jwtHelper: JwtHelperService, private loginService: LoginService) { }

  ngOnInit(): void {
    this.signed = this.loginService.isUserAuthenticated();
    this.firstname = localStorage.getItem("first_name");
  }


  logOut() {
    localStorage.removeItem("jwt");
    localStorage.removeItem("first_name");
    this.signed = this.loginService.isUserAuthenticated();
    this.router.navigate(["/"]);
  }
}
