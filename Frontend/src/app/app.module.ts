import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MenuComponent } from './menu/menu-component/menu/menu.component';
import { UserComponent } from './user/user/user.component';
import { LoginComponent } from './user/login/login.component'
import { BrowseComponent } from './game/browse/browse.component';
import { HomeComponent } from './home/home/home.component';
import { GameComponent } from './game/game/game.component';
import { AddComponent } from './game/add/add.component';
import { DeleteModalComponent } from './game/helpers/modals/delete-modal/delete-modal.component';
import { EditModalComponent } from './game/helpers/modals/edit-modal/edit-modal.component';
import { RegisterComponent } from './user/register/register.component';

import { AppRoutingModule } from './app-routing-module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtModule } from '@auth0/angular-jwt';

export function tokenGetter() {
  return localStorage.getItem("jwt");
}

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    UserComponent,
    LoginComponent,
    BrowseComponent,
    HomeComponent,
    GameComponent,
    AddComponent,
    DeleteModalComponent,
    EditModalComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    BrowserAnimationsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:5000"],
        disallowedRoutes: []
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }