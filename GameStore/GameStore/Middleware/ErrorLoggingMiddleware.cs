﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Middleware
{
    public class ErrorLoggingMiddleware
    {
        private readonly RequestDelegate _next;


        public ErrorLoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }


        public async Task Invoke(HttpContext httpContext, ILoggingService loggingService)
        {
            await loggingService.LogRequest(httpContext.Request);

            try
            {
                await _next(httpContext);
            }
            catch(Exception ex)
            {
                await loggingService.LogException(httpContext.Request, ex);
            }
        }
    }
    public static class ErrorLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseErrorLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorLoggingMiddleware>();
        }
    }

}

