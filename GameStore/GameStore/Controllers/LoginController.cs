﻿using GameStore.Models.Dtos;
using GameStore.Services.Helpers;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController :Controller
    {
        private readonly ILoginService _loginService;
        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost]
        public async Task<ActionResult<LoginResponse>> Login([FromBody]LoginDto loginDto)

        {
            var loginResponse = new LoginResponse();

            var tokenString = await _loginService.LoginUser(loginDto.Username, loginDto.Password);
            if (tokenString == null)
            {
                loginResponse.Code = 1; //failed
                loginResponse.Text = "Username or password are invalid!";
                return loginResponse;
            }

            var userDto = await _loginService.GetUserDto(loginDto.Username);
            loginResponse.Code = 0; //success
            loginResponse.UserDto = userDto;
            loginResponse.Token = tokenString;
            loginResponse.Text = "Logged in successfuly!";

            return loginResponse;
        }
    }
}
