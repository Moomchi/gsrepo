﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameUserController : Controller
    {
        private readonly IGameUserService _gameUserService;
        public GameUserController(IGameUserService gameUserService)
        {
            _gameUserService = gameUserService;
        }

        [HttpGet("{gameId}/{userid}")]
        public async Task<ActionResult<GameUser>> GetGameUser(int gameId,int userId)
        {
            var gameUser = await _gameUserService.GetUserGameById(gameId, userId);
            if (gameUser == null)
                throw new Exception("Game - user relation not found!");
            return gameUser;
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddGameUser([FromBody] GameUser info)
        {
            var message = await _gameUserService.AddGameUser(info);

            return Ok(message);
        }

        [HttpDelete("delete/{gameId}/{userId}")]
        public async Task<IActionResult> Delete(int gameId, int userId)
        {
            var gameUser = await _gameUserService.GetUserGameById(gameId, userId);
            if (gameUser == null)
                throw new Exception("Game - user relation not found!");

            await _gameUserService.DeleteGameUser(gameId, userId);
            return Ok("Deleted successfully");
        }
    }
}
