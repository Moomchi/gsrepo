﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LanguageController : NomenclatureController<Language>
    {
        private readonly ILanguageService _languageService;
        public LanguageController(INomenclatureService<Language> nomenclatureService, ILanguageService languageService)
            : base(nomenclatureService)
        {
            _languageService = languageService;
        }

        [HttpGet("games/{id}")]
        public async Task<ActionResult<List<Game>>> GetAllGames(int id)
        {
            var language = await _languageService.GetLanguageById(id);
            if (language == null)
                throw new Exception("Language not found!");
            return await _languageService.GetAllGames(id);
        }
    }
}
