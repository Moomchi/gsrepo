﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameLanguageController : Controller
    {
        private readonly IGameLanguageService _gameLanguageService;
        public GameLanguageController(IGameLanguageService gameLanguageService)
        {
            _gameLanguageService = gameLanguageService;
        }


        [HttpGet("{gameId}/{languageId}")]
        public async Task<ActionResult<GameLanguage>> GetGameLanguage(int gameId, int languageId)
        {
            var gameLanguage = await _gameLanguageService.GetGameLanguageById(gameId, languageId);
            if (gameLanguage == null)
                throw new Exception("Game - language relation not found!");
            return gameLanguage;
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddGameUser([FromBody] GameLanguage info)
        {
            var message = await _gameLanguageService.AddGameLanguage(info);

            return Ok(message);
        }

        [HttpDelete("delete/{gameId}/{languageId}")]
        public async Task<IActionResult> Delete(int gameId, int languageId)
        {
            var gameLanguage = await _gameLanguageService.GetGameLanguageById(gameId, languageId);
            if (gameLanguage == null)
                throw new Exception("Game - language relation not found!");

            await _gameLanguageService.DeleteGameLanguage(gameId, languageId);
            return Ok("Deleted successfully");
        }
    }
}
