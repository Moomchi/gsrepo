﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReviewController : Controller
    {
        private readonly IReviewService _reviewService;
        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ReviewDto>> GetReview(int id)
        {
            var review = await _reviewService.GetReviewById(id);
            if (review == null)
                throw new Exception("Review not found!");
            return review;
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddReview([FromBody] Review info)
        {
            var message = await _reviewService.AddReview(info);

            return Ok(message);
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var review = await _reviewService.GetReviewById(id);
            if (review == null)
                throw new Exception("Review not found!");

            await _reviewService.DeleteReview(review.Id);
            return Ok("Deleted successfully");
        }
    }
}
