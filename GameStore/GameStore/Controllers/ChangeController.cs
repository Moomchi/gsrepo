﻿using GameStore.Models.Dtos;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChangeController :Controller
    {
        private readonly IChangeService _changeService;
        public ChangeController(IChangeService changeService)
        {
            _changeService = changeService;
        }

        [HttpPut("names")]
        public async Task<IActionResult> UpdateNames([FromBody] ChangeNamesDto info)
        {
            string message="Unsuccessful profile update!";
            var success = await _changeService.UpdateNames(info);
            if (success)
            {
                message = "Profile updated successfully!";
            }
            return Ok(message);
        }

        [HttpPut("password")]
        public async Task<IActionResult> UpdatePassword([FromBody] ChangePasswordDto info)
        {
            string message = "Unsuccessful password update!";
            if(info.NewPassword1 == info.NewPassword2)
            {
                var success = await _changeService.UpdatePassword(info);
                if (success)
                {
                    message = "Password updated successfully!";
                }
            }
            return Ok(message);

        }
    }
}
