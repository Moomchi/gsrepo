﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Models.Enums;
using GameStore.Services.Helpers;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GameController : Controller
    {
        private readonly IGameService _gameService;
        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Game>> GetGameById(int id)
        {
            var game = await _gameService.GetGameById(id);
            if (game == null)
                throw new Exception("Game not found!");
            return game;
        }

        [HttpGet("all")]
        [Authorize(Roles ="client")]
        public async Task<ActionResult<List<Game>>> GetAll()
        {
            return await _gameService.GetAll();
        }

        [HttpGet("filtered")]
        public async Task<ActionResult<List<Game>>> GetFilteredGames([FromQuery]FilterGame filterGame)
        {
            return await _gameService.GetFilteredGames(filterGame);
        }

        [HttpGet("users/{id}")]
        public async Task<ActionResult<List<UserDtoSmall>>> GetAllUsers(int id)
        {
            var game = await _gameService.GetGameById(id);
            if (game == null)
                throw new Exception("Game not found!");
            return await _gameService.GetAllUsers(id);
        }

        [HttpGet("languages/{id}")]
        public async Task<ActionResult<List<Language>>> GetAllLanguages(int id)
        {
            var game = await _gameService.GetGameById(id);
            if (game == null)
                throw new Exception("Game not found!");
            return await _gameService.GettAllLanguages(id);
        }

        [HttpGet("reviews/{id}")]
        public async Task<ActionResult<List<ReviewDto>>> GetAllReviews(int id)
        {
            var game = await _gameService.GetGameById(id);
            if (game == null)
                throw new Exception("Game not found!");
            return await _gameService.GetAllReviews(id);
        }


        [HttpPost("add")]
        public async Task<ActionResult<Message>> AddGame([FromBody] Game info)
        {
            var message = await _gameService.AddGame(info);

            return message;
        }
        [HttpPut("edit")]
        public async Task<ActionResult<Message>> EditGame([FromBody] Game info)
        {
            var message = await _gameService.EditGame(info);

            return message;
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult<Message>> Delete(int id)
        {
            var game = await _gameService.GetGameById(id);
            if (game == null)
                throw new Exception("Game not found!");

            var message = await _gameService.DeleteGame(game.Id);
            return message;
        }
    }
}
