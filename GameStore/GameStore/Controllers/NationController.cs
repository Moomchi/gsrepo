﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NationController : NomenclatureController<Nation>
    {
        private readonly INationService _nationService;
        public NationController(INomenclatureService<Nation> nomenclatureService, INationService nationService)
            : base(nomenclatureService)
        {
            _nationService = nationService;
        }

        [HttpGet("companies/{id}")]
        public async Task<ActionResult<List<Company>>> GetAllCompanies(int id)
        {
            var nation = await _nationService.GetNationById(id);
            if (nation == null)
                 throw new Exception("Nation not found!");
            return await _nationService.GetAllCompanies(id);
        }

    }
}
