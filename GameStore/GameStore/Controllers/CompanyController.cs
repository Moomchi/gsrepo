﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CompanyController : NomenclatureController<Company>
    {
        private readonly ICompanyService _companyService;
        public CompanyController(INomenclatureService<Company> nomenclatureservice, ICompanyService companyService)
            :base(nomenclatureservice)
        {
            _companyService = companyService;
        }

        [HttpGet("all/{id}")]
        public async Task<ActionResult<List<Game>>> GetAllOfCompanyById(int id)
        {
            var company = await _companyService.GetCompanyById(id);
            if (company == null)
                throw new Exception("Company not found!");
            return await _companyService.GetAllOfCompanyById(id);
        }

    }
}
