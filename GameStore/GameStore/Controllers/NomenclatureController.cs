﻿using GameStore.Nomenclatures;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NomenclatureController<T> : Controller where T : Nomenclature
    {
        private readonly INomenclatureService<T> _nomenclatureService;
        public NomenclatureController(INomenclatureService<T> nomenclatureService)
        {
            _nomenclatureService = nomenclatureService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<T>> GetId(int id)
        {
            var element = await _nomenclatureService.GetById(id);
            if (element == null)
                 throw new Exception(typeof(T).Name+" not found!");
            return element;
        }
        [HttpGet("all")]
        public async Task<ActionResult<List<T>>> GetAll()
        {
            return await _nomenclatureService.GetAll();
        }

        [HttpPost("add")]
        public async Task<IActionResult> Add([FromBody] T info)
        {
            var message = await _nomenclatureService.Add(info);

            return Ok(message);
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var element = await _nomenclatureService.GetById(id);
            if (element == null)
                throw new Exception(typeof(T).Name + " not found!");

            await _nomenclatureService.Delete(element.Id);
            return Ok("Deleted successfully");
        }
    }
}
