﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GenreController : NomenclatureController<Genre>
    {
        private readonly IGenreService _genreService;
        public GenreController(INomenclatureService<Genre> nomenclatureService, IGenreService genreService) 
            : base(nomenclatureService)
        {
            _genreService = genreService;
        }
    }
}
