﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.Helpers;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RegistrationController : Controller
    {
        private readonly IRegistrationService _registrationService;
        public RegistrationController(IRegistrationService registrationService)
        {
            _registrationService = registrationService;
        }

        [HttpPost("create")]
        public async Task<ActionResult<Message>> CreateUser([FromBody] UserDto user)
        {
            var message = await _registrationService.UserExists(user.Email, user.Username);
            if (message.Code == 1)
                return message;

            return await _registrationService.CreateUser(user);
        }
    }
}
