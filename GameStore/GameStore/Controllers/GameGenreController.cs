﻿using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameGenreController : Controller
    {
        private readonly IGameGenreService _gameGenreService;
        public GameGenreController(IGameGenreService gameGenreService)
        {
            _gameGenreService = gameGenreService;
        }
    }
}
