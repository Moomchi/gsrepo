﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDtoSmall>> GetUserById(int id)
        {
            var user = await _userService.GetUserById(id);
            if (user == null)
                throw new Exception("User not found!");
            return user;
        }

        [HttpGet("games/{id}")]
        public async Task<ActionResult<UserGamesDto>> GetAllGames(int id)
        {
            var game = await _userService.GetUserById(id);
            if (game == null)
                throw new Exception("User not found!");
            return await _userService.GetAllGames(id);
        }

        [HttpGet("reviews/{id}")]
        public async Task<ActionResult<UserReviewDto>> GetAllReviews(int id)
        {
            var user = await _userService.GetUserById(id);
            if (user == null)
                throw new Exception("User not found!");
            return await _userService.GetAllReviews(id);
        }

    }
}
