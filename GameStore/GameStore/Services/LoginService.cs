﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.Helpers;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class LoginService :ILoginService
    {
        private readonly GameContext _context;
        public LoginService(GameContext context)
        {
            _context = context;
        }
        public async Task<User> GetUserByUsername(string username)
        {
            return await _context.Users
                                .Where(u => u.Username == username)
                                .Include(u=>u.Reviews)
                                .Include(u=>u.GameUser)
                                .FirstOrDefaultAsync();
        }

        public async Task<UserWithGamesReviews> GetUserDto(string username)
        {
            var user = await GetUserByUsername(username);
            var ids = new List<int> { };
            var reviews = new List<ReviewSmallDto>();

            if (user.Reviews !=null)
            { 
                var fullReviews = user.Reviews;

                foreach (var review in fullReviews)
                {
                    reviews.Add(new ReviewSmallDto
                    {
                        Id = review.Id,
                        Text = review.Text,
                        GameId = review.GameId

                    });
                }
            }
            if (user.GameUser != null)
            {
                foreach (var connection in user.GameUser)
                {
                    ids.Add(connection.GameId);
                }
            }

            var userDto = new UserWithGamesReviews
            {
                Id = user.Id,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                Username = user.Username,
                Age = user.Age,
                Email = user.Email,
                Role=user.Role,
                ReviewSmallDtos = reviews,
                GameIds = ids
            };
            return userDto;
    }

        public async Task<string> LoginUser(string username, string password)
        {
            var user = await GetUserByUsername(username);
            if (user == null)
            {
                return null;
            }

            var salt = user.Salt;
            var hashed = HashHelper.PrepareHashed(salt, password);
            if (hashed == user.Hashed)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,user.Email),
                    new Claim(ClaimTypes.Role,user.Role.ToString())
                };

                var tokenOptions = new JwtSecurityToken(
                    issuer: "http://localhost:5000",
                    audience: "http://localhost:5000",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
                return tokenString;
            };
            return null;
        }
    }
}
