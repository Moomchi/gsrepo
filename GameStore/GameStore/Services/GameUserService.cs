﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class GameUserService : IGameUserService
    {
        private readonly GameContext _context;
        public GameUserService(GameContext context)
        {
            _context = context;
        }

        public async Task<GameUser> GetUserGameById(int gameId,int userId)
        {
            return await _context.GameUsers
                                .Where(gu => gu.GameId == gameId && gu.UserId == userId)
                                .Include(g=>g.Game)
                                .FirstOrDefaultAsync();
        }

        public async Task<bool> GameAndUserExist(GameUser gameUser)
        {
            var gameExists =await _context.Games
                                        .Where(g => g.Id == gameUser.GameId)
                                        .FirstOrDefaultAsync() != null ? true : false;
            var userExists =await  _context.Users
                                        .Where(u => u.Id == gameUser.UserId)
                                        .FirstOrDefaultAsync() != null ? true : false;
            if (gameExists && userExists)
                return true;
            return false;

        }

        public async Task<string> AddGameUser(GameUser gameUser)
        {
            var existing = await _context.GameUsers
                                .Where(gu => gu.GameId == gameUser.GameId && gu.UserId == gameUser.UserId)
                                .FirstOrDefaultAsync() != null ? true : false;
            if (!existing && await GameAndUserExist(gameUser))
            {
                await _context.GameUsers.AddAsync(gameUser);
                await _context.SaveChangesAsync();
                return "Game - User connection added successfully!";
            }
            return "Game - User connection already exists or your input is invalid!";
        }
        public async Task DeleteGameUser(int gameId,int userId)
        {
            var gameUser = await GetUserGameById(gameId, userId);
            _context.Remove(gameUser);
            await _context.SaveChangesAsync();
        }

    }
}
