﻿using GameStore.Nomenclatures;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class NomenclatureService<T> : INomenclatureService<T> where T: Nomenclature
    {
        private readonly GameContext _context;
        public NomenclatureService(GameContext context)
        {
            _context = context;
        }

        public async Task<T> GetById(int id)
        {
            return await _context.Set<T>()
                                .Where(el => el.Id == id)
                                .FirstOrDefaultAsync();
        }

        public async Task<List<T>> GetAll()
        {
            return await _context.Set<T>()
                                    .ToListAsync();

        }
        public async Task<bool> Exists(string name)
        {
            return await _context.Set<T>()
                                    .Where(el => el.Name == name)
                                    .FirstOrDefaultAsync() != null ? true : false;
        }
        public async Task<string> Add(T element)
        {
            if (!await Exists(element.Name))
            {
                await _context.AddAsync(element);
                await _context.SaveChangesAsync();
                return typeof(T).Name.ToString() +" added successfully!";
            }
            return typeof(T).Name.ToString() + " already exists!";
        }
        public async Task Delete(int id)
        {
            var element = await GetById(id);
            _context.Remove(element);
            await _context.SaveChangesAsync();
        }

    }
}
