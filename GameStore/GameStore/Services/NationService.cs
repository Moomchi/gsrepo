﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class NationService : INationService
    {
        private readonly GameContext _context;
        public NationService(GameContext context)
        {
            _context = context;
        }

        public async Task<Nation> GetNationById(int id)
        {
            return await _context.Nations
                                .Where(n => n.Id == id)
                                .FirstOrDefaultAsync();
        }

        public async Task<List<Company>> GetAllCompanies(int id)
        {
            return await _context.Companies
                               .Where(c => c.NationId == id)
                               .ToListAsync();

        }
    }
}
