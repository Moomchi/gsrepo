﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class GameLanguageService : IGameLanguageService
    {
        private readonly GameContext _context;

        public GameLanguageService(GameContext context)
        {
            _context = context;
        }
        public async Task<GameLanguage> GetGameLanguageById(int gameId, int languageId)
        {
            return await _context.GameLanguages
                                .Where(gl => gl.GameId == gameId && gl.LanguageId == languageId)
                                .Include(g => g.Game)
                                .Include(l=>l.Language)
                                .FirstOrDefaultAsync();
        }

        public async Task<bool> GameAndLanguageExist(GameLanguage gameLanguage)
        {
            var gameExists = await _context.Games
                                        .Where(g => g.Id == gameLanguage.GameId)
                                        .FirstOrDefaultAsync() != null ? true : false;
            var languageExists = await _context.Languages
                                        .Where(l => l.Id == gameLanguage.LanguageId)
                                        .FirstOrDefaultAsync() != null ? true : false;
            if (gameExists && languageExists)
                return true;
            return false;
        }

        public async Task<string> AddGameLanguage(GameLanguage gameLanguage)
        {
            var existing = await _context.GameLanguages
                                .Where(gl => gl.GameId == gameLanguage.GameId && gl.LanguageId == gameLanguage.LanguageId)
                                .FirstOrDefaultAsync() != null ? true : false;

            if (!existing && await GameAndLanguageExist(gameLanguage))
            {
                await _context.GameLanguages.AddAsync(gameLanguage);
                await _context.SaveChangesAsync();
                return "Game - Language connection added successfully!";
            }
            return "Game - Language connection already exists or your input is invalid!";
        }
        public async Task DeleteGameLanguage(int gameId, int languageId)
        {
            var gameLanguage = await GetGameLanguageById(gameId, languageId);
            _context.Remove(gameLanguage);
            await _context.SaveChangesAsync();
        }
    }
}
