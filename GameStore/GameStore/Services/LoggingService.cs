﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class LoggingService : ILoggingService
    {
        private readonly GameContext _context;
        public LoggingService(GameContext context)
        {
            _context = context;
        }

        public async Task LogRequest(HttpRequest httpRequest)
        {
            var log = new Log()
            {
                Type = Models.Enums.LogType.TraceLog,
                LogDate = DateTime.Now,
                Verb = httpRequest.Method,
                Url = httpRequest.Path
            };
            await _context.Logs.AddAsync(log);
            _context.SaveChanges();
        }

        public async Task LogException(HttpRequest httpRequest, Exception ex)
        {
            var log = new Log()
            {
                Type = Models.Enums.LogType.ExceptionLog,
                LogDate = DateTime.Now,
                Verb = httpRequest.Method,
                Url = httpRequest.Path,
                Message = ex.Message
            };
            await _context.Logs.AddAsync(log);
            _context.SaveChanges();
        }

    }


}
