﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly GameContext _context;
        public CompanyService(GameContext context)
        {
            _context = context;
        }
        public async Task<Company> GetCompanyById(int id)
        {
            return await _context.Companies
                                    .Where(c => c.Id == id)
                                    .Include(c => c.Nation)
                                    .FirstOrDefaultAsync();
        }

        public async Task<List<Game>> GetAllOfCompanyById(int id)
        {
            return await _context.Games
                                    .Where(g => g.CompanyId == id)
                                    .ToListAsync();

        }
    }
}
