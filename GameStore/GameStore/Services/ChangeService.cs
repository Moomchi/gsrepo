﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.Helpers;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class ChangeService : IChangeService
    {
        private readonly GameContext _context;
        public ChangeService(GameContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _context.Users
                                    .Where(u => u.Email == email)
                                    .FirstOrDefaultAsync();
            }

        public async Task<bool> UpdateNames(ChangeNamesDto info)
        {
            var user = await GetUserByEmail(info.Email);
            if (user != null)
            {
                var salt = user.Salt;
                
                var hashed = HashHelper.PrepareHashed(salt, info.Password);
                if (hashed == user.Hashed)
                {
                    user.FirstName = info.FirstName;
                    user.MiddleName = info.MiddleName;
                    user.LastName = info.LastName;
                    user.Age = info.Age;
                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }
        public async Task<bool> UpdatePassword(ChangePasswordDto info)
        {
            var user = await GetUserByEmail(info.Email);
            if (user != null)
            {
                var oldPassword = HashHelper.PrepareHashed(user.Salt, info.OldPassword);
                if (oldPassword == user.Hashed)
                {
                    var salt = HashHelper.PrepareSalt();
                    var hashed = HashHelper.PrepareHashed(salt, info.NewPassword1);
                    user.Salt = salt;
                    user.Hashed = hashed;
                    await _context.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }
    }
}
