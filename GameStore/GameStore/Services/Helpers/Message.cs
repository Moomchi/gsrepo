﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.Helpers
{
    public class Message
    {
        public int Code { get; set; }
        public string Text { get; set; }
    }
}
