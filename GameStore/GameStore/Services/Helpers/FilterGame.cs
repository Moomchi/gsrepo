﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.Helpers
{
    public class FilterGame
    {
        public string Name { get; set; }
        public int? ReleaseYear { get; set; }
        public decimal? Price { get; set; }
        public string CompanyName { get; set; }
    }
}
