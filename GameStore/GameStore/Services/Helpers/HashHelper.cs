﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace GameStore.Services.Helpers
{
    public static class HashHelper
    {
        public static byte[] PrepareSalt()
        {
            byte[] salt = new byte[128 / 8];
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetNonZeroBytes(salt);
            }
            return salt;
        }
        public static string PrepareHashed(byte[] salt, string password)
        {
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                                   password: password,
                                   salt: salt,
                                   prf: KeyDerivationPrf.HMACSHA256,
                                   iterationCount: 100000,
                                   numBytesRequested: 256 / 8));
        }
    }
}
