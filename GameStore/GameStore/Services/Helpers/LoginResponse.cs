﻿using GameStore.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.Helpers
{
    public class LoginResponse : Message
    {
        public string Token { get; set; }
        public UserWithGamesReviews UserDto { get; set; }
    }
}
