﻿using GameStore.Models;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class LanguageService : ILanguageService
    {
        private readonly GameContext _context;
        public LanguageService(GameContext context)
        {
            _context = context;
        }
        public async Task<Language> GetLanguageById(int id)
        {
            return await _context.Languages
                                .Where(l => l.Id == id)
                                .FirstOrDefaultAsync();
        }

        public async Task<List<Game>> GetAllGames(int id)
        {
            var list = await _context.GameLanguages
                                    .Where(gl => gl.LanguageId == id)
                                    .Include(gl=>gl.Game)
                                        .ThenInclude(g=>g.Company)
                                            .ThenInclude(c=>c.Nation)
                                    .ToListAsync();

            var newList = new List<Game>();
            foreach (var element in list)
            {
                newList.Add(element.Game);
            }
            return newList;
        }
    }
}
