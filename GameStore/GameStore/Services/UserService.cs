﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class UserService : IUserService
    {
        private readonly GameContext _context;
        public UserService(GameContext context)
        {
            _context = context;
        }

        public async Task<UserDtoSmall> GetUserById(int id)
        {
            var user =  await _context.Users
                                .Where(n => n.Id == id)
                                .FirstOrDefaultAsync();
            if (user != null)
            {
                return new UserDtoSmall
                {
                    Id=user.Id,
                    FirstName = user.FirstName,
                    MiddleName = user.MiddleName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Age = user.Age
                };
            }
            return null;
        }

        public async Task<UserGamesDto> GetAllGames(int id)
        {
            var list =  await _context.GameUsers
                                     .Where(gu => gu.UserId == id)
                                     .Include(gu=>gu.Game)
                                     .Include(gu=>gu.Game)
                                           .ThenInclude(g=>g.Company)
                                                .ThenInclude(c=>c.Nation)
                                     .ToListAsync();

            var item = new UserGamesDto
            {
                Id = list[0].User.Id,
                FirstName = list[0].User.FirstName,
                LastName = list[0].User.LastName,
                Age = list[0].User.Age,
                Email = list[0].User.Email,
                GameDtos= new List<GameDto>()
            };

            foreach (var element in list)
            {
                var gameDto = new GameDto
                {
                    Id = element.Game.Id,
                    Name = element.Game.Name,
                    Description = element.Game.Description,
                    ReleaseYear = element.Game.ReleaseYear,
                    Price = element.Game.Price,
                    Company = element.Game.Company
                };
                item.GameDtos.Add(gameDto);
            }
            return item;
        }

        public async Task<UserReviewDto> GetAllReviews(int id)
        {
            var list = await _context.Reviews
                                    .Where(r => r.User.Id == id)
                                    .ToListAsync();

            var item = new UserReviewDto
            {
                Id = list[0].User.Id,
                FirstName = list[0].User.FirstName,
                MiddleName = list[0].User.MiddleName,
                LastName = list[0].User.LastName,
                Age = list[0].User.Age,
                Email = list[0].User.Email,
                ReviewSmallDtos = new List<ReviewSmallDto>()
            };

            foreach (var element in list)
            {
                var reviewSmallDto = new ReviewSmallDto
                {
                    Id = element.Id,
                    Text = element.Text,
                    GameId = element.GameId //ne sym sig
                };
                item.ReviewSmallDtos.Add(reviewSmallDto);
            }
            return item;
        }
    }
}
