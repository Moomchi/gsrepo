﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.Helpers;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class GameService : IGameService
    {
        private readonly GameContext _context;
        public GameService(GameContext context)
        {
            _context = context;
        }

        public async Task<Game> GetGameById(int id)
        {
            return await _context.Games
                                    .Where(g => g.Id == id)
                                    .Include(g=>g.Company)
                                        .ThenInclude(c=>c.Nation)
                                    .Include(g=>g.Reviews)
                                    .FirstOrDefaultAsync();
        }

        public async Task<List<Game>> GetAll()
        {
            return await _context.Games
                                .Include(g => g.Company)
                                        .ThenInclude(c => c.Nation)
                                .ToListAsync();
        }

        public async Task<List<Game>> GetFilteredGames(FilterGame filterGame)
        {
            var query = _context.Games
                .Include(g => g.Company)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(filterGame.Name))
            {
                query = query.Where(q => q.Name.ToLower().Contains(filterGame.Name.ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(filterGame.CompanyName))
            {
                query = query.Where(q => q.Company.Name.ToLower().Contains(filterGame.CompanyName.ToLower()));
            }
            if (filterGame.Price.HasValue)
            {
                query = query.Where(q => q.Price == filterGame.Price);
            }
            if (filterGame.ReleaseYear.HasValue)
            {
                query = query.Where(q => q.ReleaseYear == filterGame.ReleaseYear);
            }

            return await query.ToListAsync();
        }


        public async Task<List<UserDtoSmall>> GetAllUsers(int id)
        {
            var list = await _context.GameUsers
                                    .Where(gu => gu.GameId == id)
                                    .Include(gu=>gu.User)
                                    .ToListAsync();
            var newList = new List<UserDtoSmall>();
            foreach (var element in list)
            {
                var item = new UserDtoSmall
                {
                    FirstName = element.User.FirstName,
                    MiddleName = element.User.MiddleName,
                    LastName = element.User.LastName,
                    Email = element.User.Email,
                    Age = element.User.Age
                };
                newList.Add(item);
            }
            return newList;
        }

        public async Task<List<ReviewDto>> GetAllReviews(int id)
        {
            var list =  await _context.Reviews
                                        .Where(r => r.GameId == id)
                                        .Include(r=>r.User)
                                        .ToListAsync();
            if (list.Count != 0)
            {
                var item = new GameReviewDto
                {
                    Id = list[0].Game.Id,
                    Name = list[0].Game.Name,
                    Description = list[0].Game.Description,
                    ReleaseYear = list[0].Game.ReleaseYear,
                    ReviewDtos = new List<ReviewDto>()
                };

                foreach (var element in list)
                {
                    var reviewDto = new ReviewDto
                    {
                        Id = element.Id,
                        Text = element.Text,
                        UserDtoSmall = new UserDtoSmall
                        {
                            Id = element.User.Id,
                            FirstName = element.User.FirstName,
                            MiddleName = element.User.MiddleName,
                            LastName = element.User.LastName,
                            Age = element.User.Age,
                            Email = element.User.Email
                        }
                    };
                    item.ReviewDtos.Add(reviewDto);
                }
                return (List<ReviewDto>)item.ReviewDtos;
            }
            return null;
        }


        public async Task<List<Language>> GettAllLanguages(int id)
        {
            var list = await _context.GameLanguages
                                        .Where(gl => gl.GameId == id)
                                        .Include(gl => gl.Language)
                                        .ToListAsync();
            var newList = new List<Language>();
            foreach (var element in list)
            {
                newList.Add(element.Language);
            }
            return newList;
        }

        public async Task<bool> GameExists(string name)
        {
            return await _context.Games
                                    .Where(g => g.Name == name)
                                    .FirstOrDefaultAsync() != null ? true : false;
        }

        public async Task<Message> AddGame(Game game)
        {
            var message = new Message();
            if (!await GameExists(game.Name))
            {
                await _context.AddAsync(game);
                await _context.SaveChangesAsync();
                message.Code = 1;
                message.Text = "Game added successfully!";
                return message;
            }
            message.Code = 2;
            message.Text = "Game already exists!";
            return message;
        }

        public async Task<Message> EditGame(Game game)
        {
            var message = new Message();
            message.Code = 4;
            message.Text = "Game with this name already exists!";

            var oldGame = await GetGameById(game.Id);
            foreach (var item in await GetAll())
            {
                if (item.Name == game.Name && item != oldGame)
                {
                    return message;
                }
            }
            oldGame.Name = game.Name;
            oldGame.Description = game.Description;
            oldGame.Price = game.Price;
            oldGame.ReleaseYear = game.ReleaseYear;
            oldGame.LogoUrl = game.LogoUrl;
            _context.Update(oldGame);
            await _context.SaveChangesAsync();
            message.Text = "Game edited successfully!";
            message.Code = 5;

            return message;
        }

        public async Task<Message> DeleteGame(int id)
        {
            var message = new Message();
            message.Code = 0;
            message.Text = "Error while trying to delete game!";
            try
            {
                var game = await GetGameById(id);
                _context.Remove(game);
                await _context.SaveChangesAsync();
                message.Text = "Game deleted successfully!";
            }
            catch
            { 
                new Exception();
            }
            return message;
           
        }
    }
}
