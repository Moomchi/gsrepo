﻿using GameStore.Models;
using GameStore.Models.Dtos;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface ILoginService
    {
        Task<User> GetUserByUsername(string username);
        Task<string> LoginUser(string username, string password);
        Task<UserWithGamesReviews> GetUserDto(string username);
    }
}
