﻿using GameStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface IGameLanguageService
    {
        Task<GameLanguage> GetGameLanguageById(int gameId, int languageId);
        Task<bool> GameAndLanguageExist(GameLanguage gameLanguage);
        Task<string> AddGameLanguage(GameLanguage gameLanguage);
        Task DeleteGameLanguage(int gameId, int languageId);
    }
}
