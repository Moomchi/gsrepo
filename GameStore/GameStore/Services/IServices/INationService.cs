﻿using GameStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface INationService
    {
        Task<Nation> GetNationById(int id);
        Task<List<Company>> GetAllCompanies(int id);
    }
}
