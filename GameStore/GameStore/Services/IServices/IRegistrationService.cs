﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface IRegistrationService
    {
        Task<User> GetUser(int userId);
        Task<Message> UserExists(string email, string username);
        Task<Message> CreateUser(UserDto userDto);
        Task DeactivateUser(int userId);
    }
}
