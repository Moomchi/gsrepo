﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface ILoggingService
    {
        Task LogRequest(HttpRequest httpRequest);
        Task LogException(HttpRequest httpRequest, Exception ex);
    }
}
