﻿using GameStore.Models;
using GameStore.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface IUserService
    {
        Task<UserDtoSmall> GetUserById(int id);
        Task<UserGamesDto> GetAllGames(int id);
        Task<UserReviewDto> GetAllReviews(int id);
    }
}
