﻿using GameStore.Models;
using GameStore.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface IReviewService
    {
        Task<Review> GetReivew(int id);
        Task<ReviewDto> GetReviewById(int id);
        Task<bool> ReviewExists(Review review);
        Task<string> AddReview(Review review);
        Task DeleteReview(int id);
    }
}
