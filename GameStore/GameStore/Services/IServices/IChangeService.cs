﻿using GameStore.Models;
using GameStore.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface IChangeService
    {
        Task<User> GetUserByEmail(string email);
        Task<bool> UpdateNames(ChangeNamesDto changeNameDto);
        Task<bool> UpdatePassword(ChangePasswordDto changePasswordDto);
    }
}
