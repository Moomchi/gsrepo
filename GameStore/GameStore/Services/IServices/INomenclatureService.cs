﻿using GameStore.Models;
using GameStore.Nomenclatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface INomenclatureService<T> where T : Nomenclature
    {
        Task<T> GetById(int id);
        Task<List<T>> GetAll();
        Task<bool> Exists(string name);
        Task<string> Add(T element);
        Task Delete(int id);
    }
}
