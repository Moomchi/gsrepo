﻿using GameStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface IGameUserService
    {
        Task<GameUser> GetUserGameById(int gameId, int userId);
        Task<string> AddGameUser(GameUser gameUser);
        Task DeleteGameUser(int gameId, int userId);
    }
}
