﻿using GameStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface ICompanyService
    {
        Task<Company> GetCompanyById(int id);
        Task<List<Game>> GetAllOfCompanyById(int id);
    }
}
