﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface IGameService
    {
        Task<Game> GetGameById(int id);
        Task<List<Game>> GetAll();
        Task<List<Game>> GetFilteredGames(FilterGame filterGame);
        Task<List<UserDtoSmall>> GetAllUsers(int id);
        Task<List<ReviewDto>> GetAllReviews(int id);
        Task<List<Language>> GettAllLanguages(int id);
        Task<bool> GameExists(string name);
        Task<Message> AddGame(Game game);
        Task<Message> EditGame(Game game);
        Task<Message> DeleteGame(int id);
    }
}
