﻿using GameStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.IServices
{
    public interface ILanguageService
    {
        Task<Language> GetLanguageById(int id);
        Task<List<Game>> GetAllGames(int id);
    }
}
