﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.Helpers;
using GameStore.Services.IServices;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class RegistrationService : IRegistrationService
    {
        private readonly GameContext _context;
        public RegistrationService(GameContext context)
        {
            _context = context;
        }

        public async Task<User> GetUser(int userId)
        {
            return await _context.Users
                                .Where(u => u.Id == userId)
                                .FirstOrDefaultAsync();
        }
        public async Task<Message> UserExists(string email,string username)
        {
            var message = new Message();
            message.Code = 0;
            var userByEmail = await _context.Users
                                     .Where(u => u.Email == email)
                                     .FirstOrDefaultAsync();

            var userByUsername = await _context.Users
                                     .Where(u => u.Username == username)
                                     .FirstOrDefaultAsync();

            if (userByEmail != null)
            {
                message.Code = 1;
                message.Text += "Account with this email already exists! ";
            }
            if (userByUsername != null)
            {
                message.Code = 1;
                message.Text += "Account with this username already exists! ";
            }
            return message;
        }

        public async Task<Message> CreateUser(UserDto user)
        {
            var message = new Message();
           
            if (user.Password == user.Password2)
            {
                var salt = HashHelper.PrepareSalt();
                var hashed = HashHelper.PrepareHashed(salt, user.Password);
                var newUser = new User
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    MiddleName = user.MiddleName,
                    LastName = user.LastName,
                    Username = user.Username,
                    Age = user.Age,
                    Email = user.Email,
                    Role = Models.Enums.Roles.client,
                    Status = Models.Enums.Status.active,
                    Salt = salt,
                    Hashed = hashed
                };
                await _context.Users.AddAsync(newUser);
                await _context.SaveChangesAsync();
                message.Code = 0;
                message.Text = "Account created successfuly!";
                return message;
            }
            message.Code = 1;
            message.Text +="The 2 provided passwords are not the same!";
            return message;
        }
        public async Task DeactivateUser(int userId)
        {
            var user = await _context.Users
                                .Where(u => u.Id == userId)
                                .FirstOrDefaultAsync();
            user.Status = Models.Enums.Status.disabled;
            await _context.SaveChangesAsync();
        }
    }
}
