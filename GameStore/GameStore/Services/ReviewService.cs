﻿using GameStore.Models;
using GameStore.Models.Dtos;
using GameStore.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services
{
    public class ReviewService :IReviewService
    {
        private readonly GameContext _context;
        public ReviewService(GameContext context)
        {
            _context = context;
        }
        public async Task<Review> GetReivew(int id)
        {
            return await _context.Reviews
                                    .Where(r => r.Id == id)
                                    .FirstOrDefaultAsync();
        }
        public async Task<ReviewDto> GetReviewById(int id)
        {
            var review = await _context.Reviews
                                    .Where(r => r.Id == id)
                                    .Include(g => g.Game)
                                        .ThenInclude(g => g.Company)
                                            .ThenInclude(c => c.Nation)
                                    .Include(r => r.User)
                                    .FirstOrDefaultAsync();
            if (review == null)
            {
                return null;
            }

            var userDtoSmall = new UserDtoSmall
            {
                Id = review.User.Id,
                FirstName = review.User.FirstName,
                MiddleName = review.User.MiddleName,
                LastName = review.User.LastName,
                Age = review.User.Age,
                Email = review.User.Email
            };

            var reviewDto = new ReviewDto
            {
                Id = review.Id,
                Text = review.Text,
                UserDtoSmall = userDtoSmall
            };
            return reviewDto;
        }

        public async Task<bool> ReviewExists(Review review)
        {
            return await _context.Reviews
                                    .Where(r => r.Text == review.Text && r.UserId==review.UserId && r.GameId==review.GameId)
                                    .FirstOrDefaultAsync() != null ? true : false;
        }

        public async Task<bool> GameExists(int id)
        {
            return await _context.Games
                            .Where(g => g.Id == id)
                            .FirstOrDefaultAsync() != null ? true : false;
        }
        public async Task<string> AddReview(Review review)
        {
            if (await GameExists(review.GameId) &&  !await ReviewExists(review))
            {
                await _context.AddAsync(review);
                await _context.SaveChangesAsync();
                return "Review added successfully!";
            }
            return "Game does not exist in database or this review is already added!";
        }

        public async Task DeleteReview(int id)
        {
            var review = await GetReivew(id);
            _context.Remove(review);
            await _context.SaveChangesAsync();
        }
    }
}
