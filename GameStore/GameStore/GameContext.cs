﻿using Microsoft.EntityFrameworkCore;
using GameStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Nomenclatures;

namespace GameStore
{
    public class GameContext :DbContext
    {
        public GameContext()
        { 
        }
        public GameContext(DbContextOptions<GameContext> options)
           : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Nation> Nations { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<GameLanguage> GameLanguages { get; set; }
        public DbSet<GameUser> GameUsers { get; set; }
        public DbSet<GameGenre> GameGenres { get; set; }
        public DbSet<Log> Logs { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Database=GameStoreDb;Username=postgres;Password=admin");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GameUser).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GameLanguage).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GameGenre).Assembly);
        }
    }
}
