﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Models
{
    public class GameGenre
    {
        public int GameId { get; set; }
        public Game Game { get; set; }

        public int GenreId { get; set; }
        public Genre Genre { get; set; }

        class GameGenreConfiguration : IEntityTypeConfiguration<GameGenre>
        {
            public void Configure(EntityTypeBuilder<GameGenre> builder)
            {
                builder.HasKey(x => new { x.GameId, x.GenreId });
            }
        }
    }
}
