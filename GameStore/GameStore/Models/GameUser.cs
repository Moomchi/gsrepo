﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Models
{
    public class GameUser
    {
        public int GameId { get; set; }
        public Game Game { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        class GameUserConfiguration : IEntityTypeConfiguration<GameUser>
        {
            public void Configure(EntityTypeBuilder<GameUser> builder)
            {
                builder.HasKey(x => new { x.GameId, x.UserId });
            }
        }
    }
}
