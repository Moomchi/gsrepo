﻿using GameStore.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Models
{
	public class Log
	{
		public int Id { get; set; }
		public LogType Type { get; set; }
		public DateTime LogDate { get; set; }
		public string Verb { get; set; }
		public string Url { get; set; }
		public int? UserId { get; set; }
		public string Message { get; set; }
	}
}
