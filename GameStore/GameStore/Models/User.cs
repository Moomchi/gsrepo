﻿using GameStore.Models.Dtos;
using GameStore.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GameStore.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public Roles Role { get; set; }
        public Status Status { get; set; }
        public byte[] Salt { get; set; }
        public string Hashed { get; set; }

        public ICollection<Review> Reviews { get; set; }
        public ICollection<GameUser> GameUser { get; set; }

        public static Expression<Func<User, UserDtoSmall>> SelectExpression => e => new UserDtoSmall
        {
            Id = e.Id,
            FirstName = e.FirstName,
            MiddleName = e.MiddleName,
            LastName = e.LastName,
            Email = e.Email,
            Age = e.Age
        };

    }
}
