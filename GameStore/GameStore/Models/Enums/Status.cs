﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Models.Enums
{
    public enum Status
    {
        active,
        inactive,
        disabled
    }
}
