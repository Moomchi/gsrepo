﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Models
{
    public class GameLanguage
    {
        public int GameId { get; set; }
        public Game Game { get; set; }

        public int LanguageId { get; set; }
        public Language Language { get; set; }

        class GameLanguageConfiguration : IEntityTypeConfiguration<GameLanguage>
        {
            public void Configure(EntityTypeBuilder<GameLanguage> builder)
            {
                builder.HasKey(x => new { x.GameId, x.LanguageId });
            }
        }
    }
}
