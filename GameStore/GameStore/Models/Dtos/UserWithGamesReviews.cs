﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Models.Dtos
{
    public class UserWithGamesReviews :UserDtoSmall
    {
        public ICollection<ReviewSmallDto> ReviewSmallDtos { get; set; }
        public ICollection<int> GameIds { get; set; }
    }
}
