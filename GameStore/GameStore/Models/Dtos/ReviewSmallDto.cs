﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Models.Dtos
{
    public class ReviewSmallDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int GameId { get; set; }
    }
}
