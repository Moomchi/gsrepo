﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ReleaseYear { get; set; }
        public decimal Price { get; set; }
        public string LogoUrl { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }

        public ICollection<Review> Reviews { get; set; }
        public ICollection<GameUser> GameUser { get; set; }
    }
}
