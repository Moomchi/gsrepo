﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Nomenclatures
{
    public class Nomenclature
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
