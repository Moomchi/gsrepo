﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameStore.Migrations
{
    public partial class bazataapak : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Games_Prices_PriceId",
                table: "Games");

            migrationBuilder.AlterColumn<int>(
                name: "PriceId",
                table: "Games",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ComapnyId",
                table: "Games",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Prices_PriceId",
                table: "Games",
                column: "PriceId",
                principalTable: "Prices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Games_Prices_PriceId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "ComapnyId",
                table: "Games");

            migrationBuilder.AlterColumn<int>(
                name: "PriceId",
                table: "Games",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Prices_PriceId",
                table: "Games",
                column: "PriceId",
                principalTable: "Prices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
