﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameStore.Migrations
{
    public partial class priceingame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Games_Prices_PriceId",
                table: "Games");

            migrationBuilder.DropIndex(
                name: "IX_Games_PriceId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "PriceId",
                table: "Games");

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "Games",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "Games");

            migrationBuilder.AddColumn<int>(
                name: "PriceId",
                table: "Games",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Games_PriceId",
                table: "Games",
                column: "PriceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Prices_PriceId",
                table: "Games",
                column: "PriceId",
                principalTable: "Prices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
