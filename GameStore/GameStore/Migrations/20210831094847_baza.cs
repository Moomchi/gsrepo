﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameStore.Migrations
{
    public partial class baza : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Nation_NationId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Game_Company_CompanyId",
                table: "Game");

            migrationBuilder.DropForeignKey(
                name: "FK_Game_Price_PriceId",
                table: "Game");

            migrationBuilder.DropForeignKey(
                name: "FK_GameLanguage_Game_GameId",
                table: "GameLanguage");

            migrationBuilder.DropForeignKey(
                name: "FK_GameLanguage_Language_LanguageId",
                table: "GameLanguage");

            migrationBuilder.DropForeignKey(
                name: "FK_GameUser_Game_GameId",
                table: "GameUser");

            migrationBuilder.DropForeignKey(
                name: "FK_GameUser_Users_UserId",
                table: "GameUser");

            migrationBuilder.DropForeignKey(
                name: "FK_Review_Game_GameId",
                table: "Review");

            migrationBuilder.DropForeignKey(
                name: "FK_Review_Users_UserId",
                table: "Review");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Review",
                table: "Review");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Price",
                table: "Price");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Nation",
                table: "Nation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Language",
                table: "Language");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameUser",
                table: "GameUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameLanguage",
                table: "GameLanguage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Game",
                table: "Game");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Company",
                table: "Company");

            migrationBuilder.RenameTable(
                name: "Review",
                newName: "Reviews");

            migrationBuilder.RenameTable(
                name: "Price",
                newName: "Prices");

            migrationBuilder.RenameTable(
                name: "Nation",
                newName: "Nations");

            migrationBuilder.RenameTable(
                name: "Language",
                newName: "Languages");

            migrationBuilder.RenameTable(
                name: "GameUser",
                newName: "GameUsers");

            migrationBuilder.RenameTable(
                name: "GameLanguage",
                newName: "GameLanguages");

            migrationBuilder.RenameTable(
                name: "Game",
                newName: "Games");

            migrationBuilder.RenameTable(
                name: "Company",
                newName: "Companies");

            migrationBuilder.RenameIndex(
                name: "IX_Review_UserId",
                table: "Reviews",
                newName: "IX_Reviews_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Review_GameId",
                table: "Reviews",
                newName: "IX_Reviews_GameId");

            migrationBuilder.RenameIndex(
                name: "IX_GameUser_UserId",
                table: "GameUsers",
                newName: "IX_GameUsers_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_GameLanguage_LanguageId",
                table: "GameLanguages",
                newName: "IX_GameLanguages_LanguageId");

            migrationBuilder.RenameIndex(
                name: "IX_Game_PriceId",
                table: "Games",
                newName: "IX_Games_PriceId");

            migrationBuilder.RenameIndex(
                name: "IX_Game_CompanyId",
                table: "Games",
                newName: "IX_Games_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Company_NationId",
                table: "Companies",
                newName: "IX_Companies_NationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Reviews",
                table: "Reviews",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Prices",
                table: "Prices",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Nations",
                table: "Nations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Languages",
                table: "Languages",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameUsers",
                table: "GameUsers",
                columns: new[] { "GameId", "UserId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameLanguages",
                table: "GameLanguages",
                columns: new[] { "GameId", "LanguageId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Games",
                table: "Games",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Companies",
                table: "Companies",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Nations_NationId",
                table: "Companies",
                column: "NationId",
                principalTable: "Nations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GameLanguages_Games_GameId",
                table: "GameLanguages",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameLanguages_Languages_LanguageId",
                table: "GameLanguages",
                column: "LanguageId",
                principalTable: "Languages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Companies_CompanyId",
                table: "Games",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Prices_PriceId",
                table: "Games",
                column: "PriceId",
                principalTable: "Prices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GameUsers_Games_GameId",
                table: "GameUsers",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameUsers_Users_UserId",
                table: "GameUsers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reviews_Games_GameId",
                table: "Reviews",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reviews_Users_UserId",
                table: "Reviews",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Nations_NationId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_GameLanguages_Games_GameId",
                table: "GameLanguages");

            migrationBuilder.DropForeignKey(
                name: "FK_GameLanguages_Languages_LanguageId",
                table: "GameLanguages");

            migrationBuilder.DropForeignKey(
                name: "FK_Games_Companies_CompanyId",
                table: "Games");

            migrationBuilder.DropForeignKey(
                name: "FK_Games_Prices_PriceId",
                table: "Games");

            migrationBuilder.DropForeignKey(
                name: "FK_GameUsers_Games_GameId",
                table: "GameUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_GameUsers_Users_UserId",
                table: "GameUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Reviews_Games_GameId",
                table: "Reviews");

            migrationBuilder.DropForeignKey(
                name: "FK_Reviews_Users_UserId",
                table: "Reviews");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Reviews",
                table: "Reviews");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Prices",
                table: "Prices");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Nations",
                table: "Nations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Languages",
                table: "Languages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameUsers",
                table: "GameUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Games",
                table: "Games");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameLanguages",
                table: "GameLanguages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Companies",
                table: "Companies");

            migrationBuilder.RenameTable(
                name: "Reviews",
                newName: "Review");

            migrationBuilder.RenameTable(
                name: "Prices",
                newName: "Price");

            migrationBuilder.RenameTable(
                name: "Nations",
                newName: "Nation");

            migrationBuilder.RenameTable(
                name: "Languages",
                newName: "Language");

            migrationBuilder.RenameTable(
                name: "GameUsers",
                newName: "GameUser");

            migrationBuilder.RenameTable(
                name: "Games",
                newName: "Game");

            migrationBuilder.RenameTable(
                name: "GameLanguages",
                newName: "GameLanguage");

            migrationBuilder.RenameTable(
                name: "Companies",
                newName: "Company");

            migrationBuilder.RenameIndex(
                name: "IX_Reviews_UserId",
                table: "Review",
                newName: "IX_Review_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Reviews_GameId",
                table: "Review",
                newName: "IX_Review_GameId");

            migrationBuilder.RenameIndex(
                name: "IX_GameUsers_UserId",
                table: "GameUser",
                newName: "IX_GameUser_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Games_PriceId",
                table: "Game",
                newName: "IX_Game_PriceId");

            migrationBuilder.RenameIndex(
                name: "IX_Games_CompanyId",
                table: "Game",
                newName: "IX_Game_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_GameLanguages_LanguageId",
                table: "GameLanguage",
                newName: "IX_GameLanguage_LanguageId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_NationId",
                table: "Company",
                newName: "IX_Company_NationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Review",
                table: "Review",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Price",
                table: "Price",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Nation",
                table: "Nation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Language",
                table: "Language",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameUser",
                table: "GameUser",
                columns: new[] { "GameId", "UserId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Game",
                table: "Game",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameLanguage",
                table: "GameLanguage",
                columns: new[] { "GameId", "LanguageId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Company",
                table: "Company",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Nation_NationId",
                table: "Company",
                column: "NationId",
                principalTable: "Nation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Game_Company_CompanyId",
                table: "Game",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Game_Price_PriceId",
                table: "Game",
                column: "PriceId",
                principalTable: "Price",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GameLanguage_Game_GameId",
                table: "GameLanguage",
                column: "GameId",
                principalTable: "Game",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameLanguage_Language_LanguageId",
                table: "GameLanguage",
                column: "LanguageId",
                principalTable: "Language",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameUser_Game_GameId",
                table: "GameUser",
                column: "GameId",
                principalTable: "Game",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameUser_Users_UserId",
                table: "GameUser",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Review_Game_GameId",
                table: "Review",
                column: "GameId",
                principalTable: "Game",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Review_Users_UserId",
                table: "Review",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
